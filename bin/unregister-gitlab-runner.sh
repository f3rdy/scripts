#!/bin/bash

[ -z "$1" ] && { echo "Please provide project token from gitlab." ; exit 1 ; }
[ -z "$2" ] && { echo "Please provide the url (https://gitlab.com/ci) as second parameter." ; exit 1 ; }

docker exec -it gitlab-runner gitlab-runner unregister \
            --url $2 --token $1

